import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import { ngettext } from 'gettext';

import { File } from './File.js';


export const FilesView = GObject.registerClass({
	GTypeName: 'FbrFilesView',
	Template: 'resource:///org/example/filebrowser/ui/FilesView.ui',
	Properties: {
		files: GObject.ParamSpec.object(
			'files',
			'Files',
			'The list model containing the files',
			GObject.ParamFlags.READWRITE,
			Gio.ListStore
		),
		dragging: GObject.ParamSpec.boolean(
			'dragging',
			'Dragging',
			'If there is dragging happening in the view',
			GObject.ParamFlags.READWRITE,
			false
		),
	},
	InternalChildren: ['hiddenFileFilterModel'],
}, class extends Gtk.Widget {
	constructor(params={}) {
		super(params);
		this.#initializeFiles();
		this.#setupHiddenFileFilterModel();
		this.#connectToSettings();
		this.#setupActions();
	}

	#initializeFiles() {
		// Create the Gio.ListStore that will contain File objects
		this.files = Gio.ListStore.new(File);

		// Get the current directory
		const currentDir = Gio.File.new_for_path(GLib.get_current_dir());

		// Get an enumerator of all children
		const children = currentDir.enumerate_children('standard::*', Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null);

		// Iterate over the enumerator and add each child to the list store
		let fileInfo;
		while (fileInfo = children.next_file(null)) {
			this.files.append(new File({
				name: fileInfo.get_display_name(),
				icon: Gio.content_type_get_icon(fileInfo.get_content_type()),
				type: fileInfo.get_file_type(),
				gfile: children.get_child(fileInfo),
			}));
		}
	}

	#setupHiddenFileFilterModel(){
		this._hiddenFileFilterModel.filter = Gtk.CustomFilter.new(item => {
			return settings.get_boolean('show-hidden-files') ? true : item.name.charAt(0) !== '.';
		});
	}

	#connectToSettings() {
		settings.connect('changed::show-hidden-files', settings => {
			this._hiddenFileFilterModel.filter.changed(
				settings.get_boolean('show-hidden-files')
				? Gtk.FilterChange.LESS_STRICT
				: Gtk.FilterChange.MORE_STRICT
			);
		});
	}

	#setupActions() {
		const actionGroup = new Gio.SimpleActionGroup();

		const dragAction = new Gio.PropertyAction({
			name: 'drag',
			propertyName: 'dragging',
			object: this,
		});
		actionGroup.insert(dragAction);

		const copyAction = new Gio.SimpleAction({
			name: 'copy',
		});
		copyAction.connect('activate', (_action, _params) => {
			console.log(_('Copy selected files'));
		});
		actionGroup.insert(copyAction);

		this.insert_action_group('files-view', actionGroup);
	}

	onListViewActivated(listview, position) {
		console.log(_('Row activated!'));
		const item = listview.model.get_item(position);
		console.log(item.name);
	}

	onSelectionChanged(selectionmodel, _position, _n_items) {
		const selection = selectionmodel.get_selection();
		console.log(ngettext(
			'%d file selected',
			'%d files selected',
			selection.get_size()
		).replace('%d', selection.get_size()));
		for (let i = 0; i < selection.get_size(); i++) {
			const position = selection.get_nth(i);
			const item = selectionmodel.get_item(position);
			console.log(item.name);
		}
	}
});
