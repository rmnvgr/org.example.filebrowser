import Gdk from 'gi://Gdk';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk';
import GObject from 'gi://GObject';

import './FileRow.js';
import './FilesView.js';
import './WelcomeWidget.js';
import { Window } from './Window.js';


const SETTINGS_PORTAL_INTERFACE = `
<node>
	<interface name="org.freedesktop.portal.Settings">
		<method name="Read">
			<arg direction="in" type="s" name="namespace"/>
			<arg direction="in" type="s" name="key"/>
			<arg direction="out" type="v" name="value"/>
		</method>
		<signal name="SettingChanged">
			<arg type="s" name="namespace"/>
			<arg type="s" name="key"/>
			<arg type="v" name="value"/>
		</signal>
		<property name="version" access="read" type="u"/>
	</interface>
</node>
`;


export const Application = GObject.registerClass({
	GTypeName: 'FbrApplication'
}, class extends Gtk.Application {
	#settingsPortalProxy = null;

	vfunc_startup() {
		super.vfunc_startup();
		this.#loadStylesheet();
		this.#loadSettings();
		this.#setupActions();
		this.#setupAccelerators();
		this.#connectToSettingsPortal();
	}

	vfunc_activate() {
		const window = new Window({ application: this });
		window.present();
	}

	#loadStylesheet() {
		// Load the stylesheet in a CssProvider
		const provider = new Gtk.CssProvider();
		provider.load_from_resource('/org/example/filebrowser/css/style.css');

		// Add the provider to the StyleContext of the default display
		Gtk.StyleContext.add_provider_for_display(
			Gdk.Display.get_default(),
			provider,
			Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
		);
	}

	#loadSettings() {
		// Load the settings
		globalThis.settings = new Gio.Settings({ schemaId: this.applicationId });
	}

	#setupActions() {
		// Create the show-hidden-files action
		this.add_action(settings.create_action('show-hidden-files'));
	}

	#setupAccelerators() {
		// Setup accelerators
		this.set_accels_for_action('app.show-hidden-files', ['<Control>h']);
		this.set_accels_for_action('window.close', ['<Control>w']);
	}

	#connectToSettingsPortal() {
		const SettingsPortalProxy = Gio.DBusProxy.makeProxyWrapper(SETTINGS_PORTAL_INTERFACE);
		this.#settingsPortalProxy = new SettingsPortalProxy(
			Gio.DBus.session,
			'org.freedesktop.portal.Desktop',
			'/org/freedesktop/portal/desktop'
		);

		// Check that we're compatible with the settings portal
		if (this.#settingsPortalProxy.version !== 1)
			return;

		// Get the color scheme and change the appearance accordingly
		const colorScheme = this.#settingsPortalProxy.ReadSync('org.freedesktop.appearance', 'color-scheme')[0].recursiveUnpack();
		this.#changeAppearance(colorScheme);

		// Update the appearance when the color scheme changes
		this.#settingsPortalProxy.connectSignal(
			'SettingChanged',
			(_proxy, _nameOwner, [namespace, key, value]) => {
				// If this is not the setting we want that changed, return
				if (namespace !== 'org.freedesktop.appearance' || key !== 'color-scheme')
					return;
				// Unpack the value
				const colorScheme = value.recursiveUnpack();
				this.#changeAppearance(colorScheme);
			}
		);
	}

	#changeAppearance(colorScheme = 0) {
		// Possible color scheme values:
		// 0: no preference
		// 1: prefer dark appearance
		// 2: prefer light appearance
		const gtkSettings = Gtk.Settings.get_default();
		switch (colorScheme) {
			case 1:
				gtkSettings.gtkApplicationPreferDarkTheme = true;
				break;
			default:
				gtkSettings.gtkApplicationPreferDarkTheme = false;
		}
	}
});
