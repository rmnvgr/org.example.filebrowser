import Gdk from 'gi://Gdk';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import { File } from './File.js';


export const FileRow = GObject.registerClass({
	GTypeName: 'FbrFileRow',
	Template: 'resource:///org/example/filebrowser/ui/FileRow.ui',
	Properties: {
		file: GObject.ParamSpec.object(
			'file',
			'File',
			'File displayed by the row',
			GObject.ParamFlags.READWRITE,
			File
		),
	},
}, class extends Gtk.Widget {
	onDragPrepare(source, _x, _y) {
		// Set the icon to the widget
		source.set_icon(new Gtk.WidgetPaintable({ widget: this }), 0, 0);

		const providers = [];
		[[File, this.file], [Gio.File, this.file.gfile]].forEach(([gtype, object]) => {
			// Create the value
			const value = new GObject.Value();
			// Initialize the value with the object type it will hold
			value.init(gtype);
			// Put the object in the value
			value.set_object(object);
			// Add the content provider for this value to the providers
			providers.push(Gdk.ContentProvider.new_for_value(value));
		});
		// Return the union of all the providers
		return Gdk.ContentProvider.new_union(providers);
	}

	onDragBegin(_source, _drag) {
		this.activate_action('files-view.drag', null);
	}

	onDragEnd(_source, _drag, _deleteData) {
		this.activate_action('files-view.drag', null);
	}

	onDrop(_target, value, _x, _y) {
		if (value instanceof File)
			console.log(_('Move "%s" to "%s"'.replace('%s', value.name).replace('%s', this.file.name)));
		else if (value instanceof Gio.File)
			console.log(_('Move "%s" to "%s"'.replace('%s', value.get_basename()).replace('%s', this.file.name)));
		return true;
	}

	onDropAccept(_target, drop) {
		return this.file.type === Gio.FileType.DIRECTORY &&
			(drop.formats.contain_gtype(File) || drop.formats.contain_gtype(Gio.File));
	}
});
